#!/bin/python3

gdb.execute("set confirm off")
gdb.execute("target remote localhost:3333")
gdb.execute("load")
gdb.execute("monitor reset halt")
gdb.execute("delete")
gdb.execute("tbreak *main+158")
gdb.execute("continue")
result = gdb.execute("x/1dw 0xe0001004",to_string=True).replace("0xe0001004:	","")
gdb.write(result,gdb.STDERR)
