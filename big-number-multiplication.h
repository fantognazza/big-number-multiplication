#ifndef BIG_NUMBER_MULTIPLICATION
#define BIG_NUMBER_MULTIPLICATION

#include <inttypes.h>
#include <stddef.h>
#include <math.h>
#ifdef STM32L432xx
#include <stm32l4xx_hal.h>
#include <core_cm4.h>

extern RNG_HandleTypeDef hrng;
#endif

/**
 * Default parameters
 * override in Makefile
 */
#ifndef WORDSIZE
#define WORDSIZE 32                    // max dimension of registers
#endif
#ifndef BIG_NUM_BYTE_SIZE
#define BIG_NUM_BYTE_SIZE 32
#endif
#ifndef E_PARAMETER
#define E_PARAMETER 3
#endif

#define NUM_WORDS(byte_size, word_size) (ceil(byte_size*8/word_size))   // number of words composing the operands

#if WORDSIZE == 8
    typedef __uint8_t wordsize_t;
    typedef __uint16_t bigwordsize_t;
    #define WORD_MASK (0xFF)
    #define PRINT_FORMAT "%02X "
#elif WORDSIZE == 16
    typedef __uint16_t wordsize_t;
    typedef __uint32_t bigwordsize_t;
    #define WORD_MASK (0xFFFF)
    #define PRINT_FORMAT "%04X "
#elif WORDSIZE == 32
    typedef __uint32_t wordsize_t;
    typedef __uint64_t bigwordsize_t;
    #define WORD_MASK (0xFFFFFFFF)
    #ifdef STM32L432xx
        #define PRINT_FORMAT "%08lX "
    #else
        #define PRINT_FORMAT "%08X "
    #endif
#elif WORDSIZE == 64
    typedef __uint64_t wordsize_t;
    typedef __uint128_t bigwordsize_t;
    #define WORD_MASK (0xFFFFFFFFFFFFFFFF)
    #define PRINT_FORMAT "%016lX "
#endif

typedef struct triple_word_s
{
    wordsize_t field[4];
} triple_word_t;

// function prototype
uint8_t operand_scanning_multiplication(wordsize_t *A, wordsize_t *B, wordsize_t* C, size_t operands_size);
uint8_t operand_caching_multiplication(wordsize_t *A, wordsize_t *B, wordsize_t* C, size_t operands_size, uint8_t e);
uint8_t comba_multiplication(wordsize_t *A, wordsize_t *B, wordsize_t* C, size_t operands_size);

// commonly used macros

/**
 *
 * shift accumulator registers
 *
 */
#define SHIFT_ACC()                                 \
({                                                  \
    __asm__                                         \
    (                                               \
        "mov    r4, r5 \n\t"                        \
        "mov    r5, r6 \n\t"                        \
        "mov    r6, #0 \n\t"                        \
    );                                              \
})

/**
 *
 * zero the first word of the accumulator
 *
 */
#define ZERO_ACC_FIRST_WORD()                       \
({                                                  \
    __asm__                                         \
    (                                               \
        "mov    r4, #0 \n\t"                        \
    );                                              \
})

/**
 *
 * Does an addition of a bigword and checks the overflow
 *
 */
#define ADD_BIGWORD_TO_ACCUMULATOR(A,B)             \
({                                                  \
    __asm__                                         \
    (                                               \
        "umull  r0, r1, %0, %1 \n\t"                \
        "adds   r4, r4, r0 \n\t"                    \
        "adcs   r5, r5, r1 \n\t"                    \
        "IT CS \n\t"                                \
        "addcs  r6, r6, #1 \n\t"                    \
        : /* no output */                           \
        : "r" (A), "r" (B)                          \
        : "r0", "r1", "r4", "r5", "r6"              \
    );                                              \
})

/**
 *
 * Does an addition of a word and checks the overflow
 *
 */
#define ADD_WORD_TO_ACCUMULATOR(WORD)               \
({                                                  \
    __asm__                                         \
    (                                               \
        "adds   r4, r4, %0 \n\t"                    \
        "IT CS \n\t"                                \
        "addcss r5, r5, #1 \n\t"                    \
        "IT CS \n\t"                                \
        "addcs  r6, r6, #1 \n\t"                    \
        : /* no output */                           \
        : "r" (WORD)                                \
        : "r4", "r5", "r6"                          \
    );                                              \
})

#endif // BIG_NUMBER_MULTIPLICATION
