#include "big-number-multiplication.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <malloc.h>


/**
 * OPERANDS CHECK AND INITIALIZATION
 * 
 * Common routine for checking operands sanity
 * 
 */
uint8_t operands_check_and_initialization(wordsize_t *A,wordsize_t *B,wordsize_t *C, size_t operands_size)
{
    // check if operands and result memory space is already allocated
    if (!A || !B || !C)
        return(1);

    if (operands_size <= 0)
        return(2);

    // check if operands and result memory space have the correct size
    if ( (operands_size > malloc_usable_size(A)) || (operands_size > malloc_usable_size(B)) || (2*operands_size > malloc_usable_size(C) ) )
        return(3);
    
    // erease the result memory space
    memset(C,0,2*operands_size);
    return(0);
}


/**
 * OPERAND SCANNING MULTIPLICATION
 * 
 * Schoolbook multiplication
 * 
 */
uint8_t operand_scanning_multiplication(wordsize_t *A, wordsize_t *B, wordsize_t* C, size_t operands_size)
{
    uint8_t result = operands_check_and_initialization(A,B,C, operands_size);
    if (result)
        return(result);
    
    bigwordsize_t mult = 0; // its size must be twice as long as the operands
    wordsize_t tmp = 0;
    wordsize_t reminder = 0;
    uint16_t i,j = 0;

    // operand scanning algorithm
    for (i=0; i<NUM_WORDS(operands_size,WORDSIZE); i++)
    {
        for (j=0; j<NUM_WORDS(operands_size,WORDSIZE); j++)
        {
            // add reminder from previous computation
            if ((wordsize_t)(*(C+i+j) + reminder) >= *(C+i+j))
            {
                *(C+i+j) += reminder;
                reminder = 0;
            }
            else
            {
                *(C+i+j) += reminder;
                reminder = 1;
            }
            
            // compute the sub multiplication
            mult = (bigwordsize_t)*(A+i) * (bigwordsize_t)*(B+j);
            tmp = mult & WORD_MASK;
            reminder += (mult >> WORDSIZE);
            
            // add the computed sub multiplication
            if ((wordsize_t)(*(C+i+j) + tmp) >= *(C+i+j))
                *(C+i+j) += tmp;
            else // overflow detected
            {
                *(C+i+j) += tmp;
                reminder++; // TODO: Is there a case in which the reminder variable can overflow?
            }
        }
        // add reminder from previous cycle
        if ((i+j) < 2*NUM_WORDS(operands_size,WORDSIZE))    // prevent overflow
            *(C+i+j) += reminder;   // guaranteeded to be unitialized
        reminder = 0;
    }
    return(0);
}


/**
 * OPERAND CACHING MULTIPLICATION
 * 
 * M. Hutter, E. Wenger multiplication
 * 
 * PARAMETERS:
 * n: word size
 * e: loaded words per operand (n >= e)
 * r: number of ROWs to cycle
 * acc: three-words accumulator
 * 
 */
uint8_t operand_caching_multiplication(wordsize_t *A, wordsize_t *B, wordsize_t* C, size_t operands_size, uint8_t e)
{
    uint8_t result = operands_check_and_initialization(A,B,C, operands_size);
    if (result)
        return(result);

    // force the e parameter to 3
    e = 3;

    uint16_t n = NUM_WORDS(operands_size,WORDSIZE);
    uint16_t r = floor(n/e);

    int cond;

    // accumulation words
    register wordsize_t acc0 __asm__ ("r4") __attribute__((unused)) = 0;
    register wordsize_t acc1 __asm__ ("r5") __attribute__((unused)) = 0;
    register wordsize_t acc2 __asm__ ("r6") __attribute__((unused)) = 0;

    // register keywords gives a hint to compiler to keep the variable in register context without moving it to memory
    register wordsize_t a0 __asm__ ("r7") __attribute__((unused)) = 0;
    register wordsize_t a1 __asm__ ("r8") __attribute__((unused)) = 0;
    register wordsize_t a2 __asm__ ("r9") __attribute__((unused)) = 0;  // a.k.a. sb
    register wordsize_t b0 __asm__ ("r10") __attribute__((unused)) = 0; // a.k.a. sl
    register wordsize_t b1 __asm__ ("r11") __attribute__((unused)) = 0; // a.k.a. fp
    register wordsize_t b2 __asm__ ("r12") __attribute__((unused)) = 0; // a.k.a. ip


    /**
     * 
     * b_init
     * 
     */
    // load the words used for the entire b_init procedure
    cond = n-r*e-1;

    if (cond == 0) // just a single spare product
    {
        // load e-1 registers
        a0 = *(A+n-1);
        b0 = *(B);

        // single element computatioj
        
        ADD_BIGWORD_TO_ACCUMULATOR(a0,b0);
        *(C+r*e) = acc0;
        SHIFT_ACC();

        *(C+2*n-r*e-1) = acc0;  // replace with a simpler incremented index
        ZERO_ACC_FIRST_WORD();
    }
    else if (cond == 1) // 2x2 spare computations
    {
        // load e-1 registers
        a0 = *(A+n-1);
        a1 = *(A+n-2);
        b0 = *(B);
        b1 = *(B+1);


        // top-right computation
        ADD_BIGWORD_TO_ACCUMULATOR(a1,b0);
        *(C+r*e) = acc0;
        SHIFT_ACC();

        ADD_BIGWORD_TO_ACCUMULATOR(a0,b0);
        ADD_BIGWORD_TO_ACCUMULATOR(a1,b1);
        *(C+r*e+1) = acc0;
        SHIFT_ACC();

        // top-left computation
        ADD_BIGWORD_TO_ACCUMULATOR(a0,b1);
        *(C+n) = acc0;

        SHIFT_ACC();

        *(C+2*n-r*e-1) = acc0;  // replace with a simpler incremented index
        ZERO_ACC_FIRST_WORD();
    }
    

    /**
     * 
     * ROW LOOP
     * 
     */
    for (int p=r-1; p>=0; p--)
    {
        /**
         * 
         * part 1
         * 
         */
        // load new set of operands
        a0 = *(A+p*e);
        a1 = *(A+p*e+1);
        a2 = *(A+p*e+2);
        b0 = *(B);
        b1 = *(B+1);
        b2 = *(B+2);

        ADD_BIGWORD_TO_ACCUMULATOR(a0,b0);
        *(C+p*e) = acc0;
        SHIFT_ACC();

        ADD_BIGWORD_TO_ACCUMULATOR(a1,b0);
        ADD_BIGWORD_TO_ACCUMULATOR(a0,b1);
        *(C+p*e+1) = acc0;
        SHIFT_ACC();

        ADD_BIGWORD_TO_ACCUMULATOR(a2,b0);
        ADD_BIGWORD_TO_ACCUMULATOR(a1,b1);
        ADD_BIGWORD_TO_ACCUMULATOR(a0,b2);
        *(C+p*e+2) = acc0;
        SHIFT_ACC();


        /**
         * 
         * part 2
         * 
         * 
         */
        cond = n-(p+1)*e-1;
        for (int i=0; i<=cond; i++)
        {
            b0 = b1;
            b1 = b2;
            b2 = *(B+e+i); // load a new B word for each cycle

            ADD_BIGWORD_TO_ACCUMULATOR(a2,b0);
            ADD_BIGWORD_TO_ACCUMULATOR(a1,b1);
            ADD_BIGWORD_TO_ACCUMULATOR(a0,b2);
            ADD_WORD_TO_ACCUMULATOR(*(C+(p+1)*e+i)); // previous ROW result

            *(C+(p+1)*e+i) = acc0;

            SHIFT_ACC();
        }


        /**
         * 
         * part 3
         * 
         * 
         */
        cond = n-(p+1)*e-1;
        for (int i=0; i<= cond; i++)
        {
            a0 = a1;
            a1 = a2;
            a2 = *(A+(p+1)*e+i); // load a new A word for each cycle

            ADD_BIGWORD_TO_ACCUMULATOR(a2,b0);
            ADD_BIGWORD_TO_ACCUMULATOR(a1,b1);
            ADD_BIGWORD_TO_ACCUMULATOR(a0,b2);
            ADD_WORD_TO_ACCUMULATOR(*(C+n+i)); // previous ROW result

            *(C+n+i) = acc0;

            SHIFT_ACC();
        }
        

        /**
         * 
         * part 4
         * 
         */
        ADD_BIGWORD_TO_ACCUMULATOR(a2,b1);
        ADD_BIGWORD_TO_ACCUMULATOR(a1,b2);
        *(C+2*n-(p+1)*e) = acc0;
        SHIFT_ACC();

        ADD_BIGWORD_TO_ACCUMULATOR(a2,b2);
        *(C+2*n-(p+1)*e+1) = acc0;
        SHIFT_ACC();

        *(C+2*n-1-p*e) = acc0;

        ZERO_ACC_FIRST_WORD();

    } // end ROW loop


    return(0);
}


uint8_t comba_multiplication(wordsize_t *A, wordsize_t *B, wordsize_t* C, size_t operands_size)
{
    uint8_t result = operands_check_and_initialization(A,B,C, operands_size);
    if (result)
        return(result);

    uint32_t n = NUM_WORDS(operands_size,WORDSIZE);

    // accumulation words
    register wordsize_t acc0 __asm__ ("r4") __attribute__((unused)) = 0;
    register wordsize_t acc1 __asm__ ("r5") __attribute__((unused)) = 0;
    register wordsize_t acc2 __asm__ ("r6") __attribute__((unused)) = 0;

    for (uint32_t i=0; i<=n-1; i++)
    {
        for (uint32_t j=0; j<=i; j++)
        {
            ADD_BIGWORD_TO_ACCUMULATOR(*(A+j),*(B+i-j));
        }

        *(C+i) = acc0;

        SHIFT_ACC();
    }
    for (uint32_t i=n; i<=2*n-2; i++)
    {
        for (uint32_t j=i-n+1; j<=n-1; j++)
        {
            ADD_BIGWORD_TO_ACCUMULATOR(*(A+j),*(B+i-j));
        }

        *(C+i) = acc0;

        SHIFT_ACC();
    }

    *(C+2*n-1) = acc0;

    return(0);
}


#if !defined(TESTING) && !defined(STM32L432xx)
int main(int argc, char* argv[])
#elif defined(STM32L432xx)
int main_algorithm()
#endif
#if (!defined(TESTING) || defined(STM32L432xx))
{
    wordsize_t *A;
    wordsize_t *B;
    wordsize_t *C;
    
    // allocate the memory for both the operands and the result
    A = (wordsize_t *)malloc(NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);
    B = (wordsize_t *)malloc(NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);
    C = (wordsize_t *)malloc(2*NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);

    if (!A || !B || !C)
    {
        printf("Error allocating the memory space\n");
        exit(1);
    }

    #if !defined(STM32L432xx)
    srand (time(NULL)); // initialize random seed
    #endif

    // initialize to zero all the arrays
    memset(A,0,NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);
    memset(B,0,NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);
    memset(C,0,2*NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);

    // initialize to random 8bit values
    for (uint16_t i=0; i<NUM_WORDS(BIG_NUM_BYTE_SIZE, 8); i++)
    {
        #if defined(STM32L432xx)
        uint32_t random_data;
        while ( HAL_RNG_GenerateRandomNumber(&hrng, &random_data) != HAL_OK);
        *((uint8_t *)A+i) = (uint8_t)random_data;           // take [7:0] bits
        *((uint8_t *)B+i) = (uint8_t)(random_data>>8);      // take [15:8] bits
        #else
        *((uint8_t *)A+i) = rand() % 256;
        *((uint8_t *)B+i) = rand() % 256;
        #endif
    }

    // printout the operands
    printf("\n\rA: ");
    for (uint16_t i=NUM_WORDS(BIG_NUM_BYTE_SIZE,WORDSIZE); i>0; i--)
        printf(PRINT_FORMAT, *(A+i-1));
    printf("\n\r\n\rB: ");
    for (uint16_t i=NUM_WORDS(BIG_NUM_BYTE_SIZE,WORDSIZE); i>0; i--)
        printf(PRINT_FORMAT, *(B+i-1));
    printf("\n\r\n\r");

    #if defined(STM32L432xx)

    ITM->LAR = 0xC5ACCE55;     // unlock access to DWT
//  ITM->TCR = 0x1;            // global enable for ITM
//  ITM->TPR = 0x1;            // first 8 stim registers have unpriv access
//  ITM->TER = 0xf;            // enable 4 stim ports
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;     // enable trace
    DWT->CYCCNT = 0;          // reset cycle counter register
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;           // enable DWT cycle counter, CPI counter and LSU counter events

    #endif

    uint8_t result = UINT8_MAX;
    #if defined(OPERAND_SCANNING)
    result = operand_scanning_multiplication(A, B, C, NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);
    #elif defined(OPERAND_CACHING)
    result = operand_caching_multiplication(A, B, C, NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8, E_PARAMETER);
    #elif defined(COMBA)
    result = comba_multiplication(A, B, C, NUM_WORDS(BIG_NUM_BYTE_SIZE, WORDSIZE) * WORDSIZE/8);
    #endif

    #ifdef STM32L432xx
    DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk;           // disable DWT cycle counter
    printf("Cycle counter: %ld\n\r", DWT->CYCCNT);
    #endif

    // printout the result
    if (result == 0)
    {
        printf("C: ");
        for (uint16_t i=2*NUM_WORDS(BIG_NUM_BYTE_SIZE,WORDSIZE); i>0; i--)
            printf(PRINT_FORMAT, *(C+i-1));
        printf("\n\r\n\r");
    }
    else if (result == UINT8_MAX)
    {
        printf("Warning: algorithm not executed\n\r");
    }
    else
    {
        printf("Big Number Multiplication returned an error! %d\n\r", result);
    }

    return(0);
}
#endif
