# Target system: armv7a
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR arm)
SET(CMAKE_SYSTEM_VERSION 1)
SET(CMAKE_CROSSCOMPILING TRUE)

# search headers and libraries in the target environment,
# search programs in the host environment
SET(CMAKE_LIBRARY_ARCHITECTURE arm-linux-gnueabihf)
SET(CMAKE_FIND_ROOT_PATH /usr/arm-linux-gnueabihf)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# Cross compiler
SET(CMAKE_C_COMPILER   arm-linux-gnueabihf-gcc)
SET(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
SET(CMAKE_ASM_COMPILER arm-linux-gnueabihf-as)
SET(CMAKE_OBJCOPY      arm-linux-gnueabihf-objcopy)
SET(CMAKE_OBJDUMP      arm-linux-gnueabihf-objdump)

# Compiler flags
SET(CMAKE_C_FLAGS "-Wall -std=c99" CACHE INTERNAL "c compiler flags")
SET(CMAKE_CXX_FLAGS "-Wall" CACHE INTERNAL "cxx compiler flags")
SET(CMAKE_EXE_LINKER_FLAGS "" CACHE INTERNAL "exe link flags")

SET(THREADS_PTHREAD_ARG "0" CACHE STRING "Result from TRY_RUN" FORCE)
