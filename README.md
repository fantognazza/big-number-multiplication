# Big number multiplication

![build status](https://gitlab.com/fantognazza/big-number-multiplication/badges/master/build.svg)

# Abstract

Research project aimed to implement _M. Hutter, E. Wenger_ operand-caching multiplication of big number and to evaluate performance gain with respect to the schoolbook operand-scanning and Comba algorithm implementations.


# Files description

* **big-number-multiplication_test.cpp**: test file

* **big-number-multiplication.c**: operand-caching, operand-scanning and Comba algorithms' source file

* **big-number-multiplication.h**: common supporting MACROs and definitions

* **gdb_commands.py**: GDB script executed on performance analysis

* **perf_evaluation.py**: script to run some perf stats and plot charts


# Results

Since the ARM Cortex-M4 core have only 16 addressable registers (14 effectively usable), the operand-caching algorithm cannot exceed the value of 3 for the parameter _e_.

Therefore, 3 words for each operand are reserved inside the CPU (for a total of 6 registers), 3 words are used to represent the accumulator and 2 registers are used for indexing un-rollable loops, leaving with just 3 free registers to use for the computations.

Nonetheless, the following benchmark shows the reduction of used clock-cycles by almost 12.5% with respect to the Comba algorithm (and 92.5% with respect to the schoolbook operand-scanning):

![performance comparison](clock_chart.svg)


# Build instructions

This branch is aimed to target those algorithms on a STM32L432KC Nucleo board.

## Algorithms

Those dependencies are required:
* make
* arm-none-eabi-gcc
* arm-none-eabi-binutils

To build examples with all the algorithms, just run:

```
$ make all
```

This command also produces in the `build` directory the firmware for STM32L432KC Nucleo-32 board. You can flash the resulting `{operand-scanning,operand-caching,comba}.bin` binaries via an ST-Link adapter.

## Tests

Those dependencies (or equivalent packages) are required:
* git
* make
* cmake
* binutils-arm-linux-gnueabihf
* gcc-arm-linux-gnueabihf
* g++-arm-linux-gnueabihf
* libgmp-dev:armhf
* libstdc++-8-dev:armhf
* qemu-user

To built the tests, just run:

```
$ make tests
```

The resulting executable can be found in `build/big-number-multiplication_test`. You can run it through qemu:

```
$ qemu-arm build/big-number-multiplication_test
```

## Performance evaluation

The provided python script makes use of the following python modules:
* matplotlib
* numpy
* lxml

Make sure also to satisfy those system requirements:
* make
* arm-none-eabi-gcc
* arm-none-eabi-binutils
* arm-none-eabi-gdb
* st-util

Then, you can execute the script:

```
$ python3 ./perf_evaluation.py
```

# Download the artifacts

* [operand-scanning](https://gitlab.com/fantognazza/big-number-multiplication/-/jobs/artifacts/master/download?job=operand-scanning)
* [operand-caching](https://gitlab.com/fantognazza/big-number-multiplication/-/jobs/artifacts/master/download?job=operand-caching)
* [comba](https://gitlab.com/fantognazza/big-number-multiplication/-/jobs/artifacts/master/download?job=comba)
* [tests](https://gitlab.com/fantognazza/big-number-multiplication/-/jobs/artifacts/master/download?job=test)
