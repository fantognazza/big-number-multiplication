#include "gtest/gtest.h"
#include <gmp.h>
#include "big-number-multiplication.h"
#include "big-number-multiplication.c"
#include <string.h>
#include <stdlib.h>
#include <time.h> 

namespace {

    using ::testing::TestWithParam;

    wordsize_t *A;
    wordsize_t *B;
    wordsize_t *C;

    class BigNumberMultiplicationTest : public TestWithParam<int> {
        protected:
            uint32_t operand_length;
            void SetUp() override
            {
                struct timespec ts;
                clock_gettime(CLOCK_MONOTONIC, &ts);
                srand((time_t)ts.tv_nsec); // initialize random seed

                // allocate operands and result memory space
                this->operand_length = NUM_WORDS((BIG_NUM_BYTE_SIZE + (rand() % BIG_NUM_BYTE_SIZE)), WORDSIZE) * WORDSIZE/8;
                A = (wordsize_t *)malloc(this->operand_length);
                B = (wordsize_t *)malloc(this->operand_length);
                C = (wordsize_t *)malloc(2*this->operand_length);

                if (!A || !B || !C)
                    exit(1);

                // initialize to zero all the arrays
                memset(A,0,this->operand_length);
                memset(B,0,this->operand_length);
                memset(C,0,2*this->operand_length);


                // initialize to random 8bit values
                for (uint16_t i=0; i<NUM_WORDS(this->operand_length, 8); i++)
                {
                    *((uint8_t *)A+i) = rand() % 256;
                    *((uint8_t *)B+i) = rand() % 256;
                }
            }

            void TearDown() override
            {
                if (A)
                    free(A);
                if (B)
                    free(B);
                if (C)
                    free(C);
            }
    };

    // test for parameter checking (run once)
    TEST_F(BigNumberMultiplicationTest, testArguments) {
        ASSERT_EQ(operands_check_and_initialization(NULL,NULL,NULL, this->operand_length),1);
        ASSERT_EQ(operands_check_and_initialization((wordsize_t *)123,NULL,NULL, this->operand_length),1);
        ASSERT_EQ(operands_check_and_initialization(NULL,(wordsize_t *)123,NULL, this->operand_length),1);
        ASSERT_EQ(operands_check_and_initialization(NULL,NULL,(wordsize_t *)123, this->operand_length),1);
        ASSERT_EQ(operands_check_and_initialization((wordsize_t *)123,(wordsize_t *)123,NULL, this->operand_length),1);
        ASSERT_EQ(operands_check_and_initialization((wordsize_t *)123,NULL,(wordsize_t *)123, this->operand_length),1);
        ASSERT_EQ(operands_check_and_initialization(NULL,(wordsize_t *)123,(wordsize_t *)123, this->operand_length),1);

        ASSERT_EQ(operands_check_and_initialization(A,B,C, 0),2);
        ASSERT_EQ(operands_check_and_initialization(A,B,C, -1),3);
        ASSERT_EQ(operands_check_and_initialization(A,B,C, this->operand_length+300),3);

        ASSERT_EQ(operands_check_and_initialization(A,B,C, this->operand_length),0);
    }

    // test for correct output (run multiple times)
    TEST_P(BigNumberMultiplicationTest, testCorrectnessOperandScanning) {
        // define GMP variables
        mpz_t gmpA, gmpB, gmpC, gmpRES;
        mpz_inits(gmpA, gmpB, gmpC, gmpRES, NULL);   

        // generate the result using the GMP library
        mpz_import(gmpA, NUM_WORDS(this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)A);
        mpz_import(gmpB, NUM_WORDS(this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)B);

        // generate the comparison value from GMP library
        mpz_mul(gmpRES, gmpA, gmpB);

        // run the tested operand scanning function and transform in GMP format
        operand_scanning_multiplication(A,B,C, this->operand_length);
        mpz_import(gmpC, NUM_WORDS(2*this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)C);

        // confront the two results
        EXPECT_EQ(mpz_cmp(gmpRES,gmpC),0) << "op_lenght: " << this->operand_length;
    }

    TEST_P(BigNumberMultiplicationTest, testCorrectnessOperandCaching) {
        // define GMP variables
        mpz_t gmpA, gmpB, gmpC, gmpRES;
        mpz_inits(gmpA, gmpB, gmpC, gmpRES, NULL);   

        // generate the result using the GMP library
        mpz_import(gmpA, NUM_WORDS(this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)A);
        mpz_import(gmpB, NUM_WORDS(this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)B);

        // generate the comparison value from GMP library
        mpz_mul(gmpRES, gmpA, gmpB);

        // run the tested operand caching function and transform in GMP format
        uint32_t e = 1 + rand() % 4;     // generate a random e = [1,5]
        operand_caching_multiplication(A,B,C,this->operand_length, e);
        mpz_import(gmpC, NUM_WORDS(2*this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)C);

        // confront the two results
        EXPECT_EQ(mpz_cmp(gmpRES,gmpC),0) << "e: " << e <<" op_lenght: " << this->operand_length;
    }

    TEST_P(BigNumberMultiplicationTest, testCorrectnessComba) {
        // define GMP variables
        mpz_t gmpA, gmpB, gmpC, gmpRES;
        mpz_inits(gmpA, gmpB, gmpC, gmpRES, NULL);   

        // generate the result using the GMP library
        mpz_import(gmpA, NUM_WORDS(this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)A);
        mpz_import(gmpB, NUM_WORDS(this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)B);

        // generate the comparison value from GMP library
        mpz_mul(gmpRES, gmpA, gmpB);

        // run the tested operand scanning function and transform in GMP format
        comba_multiplication(A,B,C, this->operand_length);
        mpz_import(gmpC, NUM_WORDS(2*this->operand_length, WORDSIZE), -1, WORDSIZE/8, 0, 0, (wordsize_t *)C);

        // confront the two results
        EXPECT_EQ(mpz_cmp(gmpRES,gmpC),0) << "op_lenght: " << this->operand_length;
    }

    // run TEST_P 3*150 times
    INSTANTIATE_TEST_SUITE_P(Instantiation, BigNumberMultiplicationTest, testing::Range(1, 151));
}

