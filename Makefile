# ------------------------------------------------
# Generic Makefile (based on gcc)
#
# ChangeLog :
#	2017-02-10 - Several enhancements + project update mode
#   2015-07-22 - first version
# ------------------------------------------------

#######################################
# paths
#######################################
# Build path
BUILD_DIR = build

######################################
# target
######################################
TARGET = $(BUILD_DIR)/operand-scanning $(BUILD_DIR)/operand-caching $(BUILD_DIR)/comba


######################################
# building variables
######################################
# debug build?
DEBUG = 1
# optimization
OPT = -O2


######################################
# source
######################################
# C sources
C_SOURCES =  \
big-number-multiplication.c \
STM32L4/Src/main.c \
STM32L4/Src/stm32l4xx_it.c \
STM32L4/Src/stm32l4xx_hal_msp.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c \
STM32L4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rng.c \
STM32L4/Src/system_stm32l4xx.c  

# ASM sources
ASM_SOURCES =  \
STM32L4/startup_stm32l432xx.s


#######################################
# binaries
#######################################
PREFIX = arm-none-eabi-
# The gcc compiler bin path can be either defined in make command via GCC_PATH variable (> make GCC_PATH=xxx)
# either it can be added to the PATH environment variable.
ifdef GCC_PATH
CC = $(GCC_PATH)/$(PREFIX)gcc
CXX = $(GCC_PATH)/$(PREFIX)g++
AS = $(GCC_PATH)/$(PREFIX)gcc -x assembler-with-cpp
CP = $(GCC_PATH)/$(PREFIX)objcopy
SZ = $(GCC_PATH)/$(PREFIX)size
else
CC = $(PREFIX)gcc
CXX = $(PREFIX)g++
AS = $(PREFIX)gcc -x assembler-with-cpp
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size
endif
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S
 
#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=cortex-m4

# fpu
FPU = -mfpu=fpv4-sp-d16

# float-abi
FLOAT-ABI = -mfloat-abi=hard

# mcu
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

# macros for gcc
# AS defines
AS_DEFS = 

# C defines
C_DEFS =  \
-DUSE_HAL_DRIVER \
-DSTM32L432xx


# AS includes
AS_INCLUDES = 

# C includes
C_INCLUDES =  \
-ISTM32L4/Inc \
-ISTM32L4/Drivers/STM32L4xx_HAL_Driver/Inc \
-ISTM32L4/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy \
-ISTM32L4/Drivers/CMSIS/Device/ST/STM32L4xx/Include \
-ISTM32L4/Drivers/CMSIS/Include

PROJ_FLAGS = -std=c99 -DWORDSIZE=32 
CXXFLAGS = -O0 -Wall -fomit-frame-pointer

# compile gcc flags
ASFLAGS = $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections

CFLAGS = $(MCU) $(C_DEFS) $(C_INCLUDES) $(OPT) $(PROJ_FLAGS) -Wall -fdata-sections -ffunction-sections
FIXED_REGS = 

ifeq ($(DEBUG), 1)
CFLAGS += -g -gdwarf-2
endif


# Generate dependency information
CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)"


#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT = STM32L4/STM32L432KCUx_FLASH.ld

# libraries
LIBS = -lc -lm -lnosys 
LIBDIR = 
LDFLAGS = $(MCU) -specs=nano.specs -T$(LDSCRIPT) $(LIBDIR) $(LIBS) -Wl,--cref -Wl,--gc-sections

# default action: build all
all: $(patsubst $(BUILD_DIR)/%, %, $(TARGET))


#######################################
# build the application
#######################################
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))
# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD_DIR)/big-number-multiplication.o: big-number-multiplication.c Makefile | $(BUILD_DIR) 
	$(CC) -c $(CFLAGS) $(FIXED_REGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR) 
	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)
	$(AS) -c $(CFLAGS) $< -o $@

$(BUILD_DIR)/%.elf: $(OBJECTS) Makefile
	$(CC) -c $(CFLAGS) $(FIXED_REGS) \
		-D$(shell echo $(patsubst $(BUILD_DIR)/%, %, $(@:%.elf=%)) | tr a-z A-Z | tr '-' '_') \
		-Wa,-a,-ad,-alms=$(BUILD_DIR)/big-number-multiplication.lst \
		big-number-multiplication.c -o $(BUILD_DIR)/big-number-multiplication.o
	$(CC) $(OBJECTS) $(LDFLAGS) -Wl,-Map=$(@:%.elf=%.map) -o $@
	$(SZ) $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(HEX) $^ $@
	
$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(BIN) $^ $@
	
$(BUILD_DIR):
	mkdir $@		

#######################################
# operand scanning
#######################################
operand-scanning: FIXED_REGS = 
operand-scanning: $(BUILD_DIR)/operand-scanning.elf $(BUILD_DIR)/operand-scanning.hex $(BUILD_DIR)/operand-scanning.bin

#######################################
# operand caching
#######################################
operand-caching: FIXED_REGS = -ffixed-r4 -ffixed-r5 -ffixed-r6 -ffixed-r7 -ffixed-r8 -ffixed-r9 -ffixed-r10 -ffixed-r11 -ffixed-r12
operand-caching: $(BUILD_DIR)/operand-caching.elf $(BUILD_DIR)/operand-caching.hex $(BUILD_DIR)/operand-caching.bin

#######################################
# comba
#######################################
comba: FIXED_REGS = -ffixed-r4 -ffixed-r5 -ffixed-r6
comba: $(BUILD_DIR)/comba.elf $(BUILD_DIR)/comba.hex $(BUILD_DIR)/comba.bin

#######################################
# clean up
#######################################
clean:
	rm -fR $(BUILD_DIR)

#######################################
# flash operand scanning algorithm
#######################################
flash_operand-scanning:
	st-flash write build/operand-scanning.bin 0x8000000

#######################################
# flash operand caching algorithm
#######################################
flash_operand-caching:
	st-flash write build/operand-caching.bin 0x8000000

#######################################
# flash comba algorithm
#######################################
flash_comba:
	st-flash write build/comba.bin 0x8000000

#######################################
# tests
#######################################
GTESTDIR=googletest
TESTLIBS= -lgtest -lgtest_main -lgmp -lpthread
TEST = big-number-multiplication_test.cpp

$(BUILD_DIR)/big-number-multiplication_test: $(TEST) $(GTESTDIR)/lib/libgtest.a
	mkdir -p $(BUILD_DIR)
	arm-linux-gnueabihf-g++ $(CXXFLAGS) -Wall \
		-I/usr/arm-linux-gnueabihf/include \
		-I/usr/include/arm-linux-gnueabihf \
		-I$(GTESTDIR)/$(GTESTDIR)/include \
		-L/usr/arm-linux-gnueabihf/lib \
		-L/usr/lib/arm-linux-gnueabihf \
		-L$(GTESTDIR)/lib $(TESTLIBS) \
		-DTESTING \
		-o $@ $^

$(GTESTDIR)/lib/libgtest.a:
	git submodule sync --recursive
	git submodule update --init --recursive
	cd $(GTESTDIR) && rm -rf CMakeCache.txt CMakeFiles && cmake ./ -DCMAKE_TOOLCHAIN_FILE=../toolchain-arm-linux-gnueabihf.cmake && make

tests: $(BUILD_DIR)/big-number-multiplication_test
  
#######################################
# dependencies
#######################################
-include $(wildcard $(BUILD_DIR)/*.d)

.PHONY: all operand-scanning operand-caching comba clean flash_operand-scanning flash_operand-caching flash_comba tests

# *** EOF ***
