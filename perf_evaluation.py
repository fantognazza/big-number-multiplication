#!/bin/python

import os
import subprocess
import re, fileinput
import matplotlib.pyplot as plt
import numpy as np
from lxml import etree

op_scanning = "./build/operand-scanning"
op_caching = "./build/operand-caching"
comba = "./build/comba"
header_file = "big-number-multiplication.h"


"""
gather the counters in arm processor
"""
def save_counters(xml, program):

    cyc_cnt = []
    openocd = subprocess.Popen(args=["st-util", "--multi", "-v0", "--listen_port=3333"])

    xml.write("        <runs>\n")
    # do multiple runs
    for i in range(1,11):
        while True:
            try:
                output = subprocess.check_output(args=["arm-none-eabi-gdb", "--se="+program+".elf", "--command=gdb_commands.py", "--batch-silent", "--nx"], stderr=subprocess.STDOUT, timeout=7).decode("UTF-8").rstrip()
            except subprocess.TimeoutExpired:
                print("Timeout reached")
                continue
            except KeyboardInterrupt:
                break
            break
        print('Run {0}: {1}'.format(i, output))
        cyc_cnt.append(int(output))
        xml.write('            <cycles>{0}</cycles>\n'.format(output))
    xml.write("        </runs>\n")

    cyc_cnt.remove(max(cyc_cnt))
    cyc_cnt.remove(min(cyc_cnt))
    mean_value = np.mean(cyc_cnt, dtype=int)
    deviation = np.std(cyc_cnt)

    xml.write("        <cpu-clock>\n")
    xml.write('            <value>{0}</value>\n'.format(mean_value))
    xml.write('            <unit>cpu-cycles</unit>\n')
    xml.write('            <error>{0:.2f}%</error>\n'.format(deviation/mean_value*100))
    xml.write("        </cpu-clock>\n")

    openocd.kill()

"""
run gather performance stats from DWT and build a structured xml as output
"""
def run_arm_test():
    # matching regexp for header file inline edit
    regex1 = re.compile(r"^#define BIG_NUM_BYTE_SIZE [0-9]+.*$")
    regex2 = re.compile(r"^#define E_PARAMETER [0-9]+.*$")

    # xml tags are added in list and at the end joined
    os.mkdir('build')
    xml_op_scanning = open(op_scanning+".xml", "w+")
    xml_op_caching = open(op_caching+".xml", "w+")
    xml_comba = open(comba+".xml", "w+")

    xml_op_scanning.write("<operand-scanning>\n")
    xml_op_caching.write("<operand-caching>\n")
    xml_comba.write("<comba>\n")

    # run multiple performance evaluation for each operand size  (EDIT RANGE TO MODIFY THE TEST)
    for op_size in range(512,2049,512):
        
        # replace the operand size in the header, compile it and run the performance evaluation
        for line in fileinput.input(header_file, inplace=True):
            line = regex1.sub("#define BIG_NUM_BYTE_SIZE %d" % op_size, line.rstrip())
            print(line)
        
        subprocess.run(args=["rm", "-f", op_scanning+".{elf,bin,hex,map}"])
        subprocess.run(args=["make", os.path.basename(op_scanning)])

        xml_op_scanning.write('    <test op_size="{0}">\n'.format(op_size))
        save_counters(xml_op_scanning, op_scanning)
        xml_op_scanning.write("    </test>\n")

        xml_op_scanning.flush()
        os.fsync(xml_op_scanning.fileno())

        # run performance evaluation for a specific e parameter value
        
        for line in fileinput.input(header_file, inplace=True):
            line = regex2.sub("#define E_PARAMETER %d" % 3, line.rstrip())
            print(line)

        subprocess.run(args=["rm", "-f", op_caching+".{elf,bin,hex,map}"])
        subprocess.run(args=["make", os.path.basename(op_caching)])
        
        # add the operand-scanning test result in the XML
        xml_op_caching.write('    <test op_size="{0}" e_param="{1}">\n'.format(op_size,3))
        save_counters(xml_op_caching, op_caching)
        xml_op_caching.write("    </test>\n")

        xml_op_caching.flush()
        os.fsync(xml_op_caching.fileno())
    
        subprocess.run(args=["rm", "-f", comba+".{elf,bin,hex,map}"])
        subprocess.run(args=["make", os.path.basename(comba)])

        # add the operand-scanning test result in the XML
        xml_comba.write('    <test op_size="{0}">\n'.format(op_size))
        save_counters(xml_comba, comba)
        xml_comba.write("    </test>\n")

        xml_comba.flush()
        os.fsync(xml_comba.fileno())
            
    # generate the XML output
    xml_op_scanning.write("</operand-scanning>\n")
    xml_op_caching.write("</operand-caching>\n")
    xml_comba.write("</comba>\n")

    xml_op_scanning.flush()
    os.fsync(xml_op_scanning.fileno())
    os.close(xml_op_scanning.fileno())
    xml_op_caching.flush()
    os.fsync(xml_op_caching.fileno())
    os.fsync(xml_op_caching.fileno())
    xml_comba.flush()
    os.fsync(xml_comba.fileno())
    os.fsync(xml_comba.fileno())

    


"""
parse operand scanning XML and generate a chart 'cpu-clock w.r.t. operand size'
"""
def clock_chart():

    tree_op_scanning = etree.parse(op_scanning+".xml")
    root_op_scanning = tree_op_scanning.getroot()

    op_scanning_results = root_op_scanning.xpath('//cpu-clock')
    op_scanning_errors = []
    op_scanning_values = []
    o_size = root_op_scanning.xpath('//test/@op_size')
    for idx,result in enumerate(op_scanning_results):
        op_scanning_values.append(float(result.find('value').text))
        op_scanning_errors.append(float(result.find('error').text[:-1])*op_scanning_values[idx]*1/100)
            
    tree_op_caching = etree.parse(op_caching+".xml")
    root_op_caching = tree_op_caching.getroot()

    op_caching_results = root_op_caching.xpath('//test[@e_param="3"]/cpu-clock')
    op_caching_errors = []
    op_caching_values = []
    for idx,result in enumerate(op_caching_results):
        op_caching_values.append(float(result.find('value').text))
        op_caching_errors.append(float(result.find('error').text[:-1])*op_scanning_values[idx]*1/100)

    tree_comba = etree.parse(comba+".xml")
    root_comba = tree_comba.getroot()

    comba_results = root_comba.xpath('//cpu-clock')
    comba_errors = []
    comba_values = []
    for idx,result in enumerate(comba_results):
        comba_values.append(float(result.find('value').text))
        comba_errors.append(float(result.find('error').text[:-1])*comba_values[idx]*1/100)

    fig, ax = plt.subplots()
    x = np.arange(len(op_scanning_values))
    bar_width = 0.25
    alpha = 0.8

    ax.bar(x-bar_width, op_scanning_values, yerr=op_scanning_errors, width=bar_width, alpha=alpha, label="operand scanning", capsize=5)
    ax.bar(x, op_caching_values, yerr=op_caching_errors, width=bar_width, alpha=alpha, label="operand caching (e=3)", capsize=5)
    ax.bar(x+bar_width, comba_values, yerr=comba_errors, width=bar_width, alpha=alpha, label="comba", capsize=5)

    ax.set_xlabel("operand length (B)")
    ax.set_ylabel(op_scanning_results[0].find('unit').text)
    ax.set_title("cpu-clock w.r.t. operand size")
    ax.set_xticks(x)
    ax.set_xticklabels(o_size)
    ax.legend().set_title("algorithm")

    fig.tight_layout()
    plt.savefig('build/clock_chart.svg')
    print("Graph saved to build/clock_chart.svg")

"""

PERFORMANCE EVALUATION

* clean the 'build' working directory
* run the evalations
* generate the charts

"""
subprocess.run(args=["make", "clean"])
run_arm_test()
clock_chart()
